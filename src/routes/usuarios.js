const { request, response } = require("express");
const express = require("express");
const router = express.Router();
const cors = require('cors');

const mysqlConnection = require("../database");

router.use(cors());
 //GET

router.get("/api/todosUsuarios", (resquest, response) => {
  mysqlConnection.query("SELECT * FROM Usuarios;",
   (err, rows, fields) => {
    if (!err) {
      response.json(rows);
    } else {
      console.log(err);
    }
  });
});

router.get("/api/soloUsuario/:id", (request, response) => {
  const id = request.params.id;

  mysqlConnection.query(
    "SELECT * FROM Usuarios WHERE idUsuarios = ?;",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json(rows);
      } else {
      }
    }
  );
});


//POST
router.post("/api/guardarUsuario", async (request, response) => {
  const idUsuario = request.body.idUsuario;
  const nombre = request.body.nombre;
  const salario = request.body.salario;
  

  mysqlConnection.query(
    "INSERT INTO Usuarios (idUsuarios, nombre, salario) VALUES (?,?,?);",
    [idUsuario, nombre, salario],
    (err, rows, fields) => {
      if (!err) {
        response.json({
            msg: "Usuario registrado correctamente"
        });
      } else {
        response.json(err);
      }
    }
  );


});

//PUT

router.put("/api/modificarUsuario/:id", (request, response) => {
    const id = request.params.id;
    const nombre = request.body.nombre;
    const salario = request.body.salario;
  
    mysqlConnection.query(
      "UPDATE Usuarios SET nombre = ?, salario = ?  WHERE idUsuarios = ?",
      [nombre, salario, id],
      (err, rows, fields) => {
        if (!err) {
          response.json({
            msg: "Usuario modificado correctamente"
          });
        } else {
          response.json(err);
        }
      }
    );


});


//DELETE

router.delete("/api/eliminarUsuario/:id", (request, response) => {
  const id = request.params.id;
  mysqlConnection.query(
    "DELETE FROM Usuarios WHERE idUsuarios = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json({
          msg: "Usuario eliminado correctamente"
        })
      } else {
        response.json(err);
      }
    }
  ); 
});

module.exports = router;
